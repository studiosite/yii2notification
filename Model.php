<?php

namespace studiosite\yii2notification;

/**
 * Виджет отрисовки уведомлений в виде всплывающих уведомлений и подробностей в модальном окне
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class Model extends \yii\base\Model
{
	const TYPE_NOTICE = 'notice';
    const TYPE_INFO = 'info';
    const TYPE_SUCCESS = 'success';
    const TYPE_ERROR = 'error';

    /**
	* @var integer Id
	*/
	public $id;

	/**
	* @var string Title
	*/
	public $title;

	/**
	* @var string Type
	*/
	public $type;

    /**
	* @var string Иконка
	*/
	public $icon = '';

	/**
	* @var string Тело сообщения
	*/
	public $message = '';

	/**
	* @var array|string|Closure[]|string[]|array[] кнопки
	*/
	public $buttons;

	/**
	* @var array Опции всплывающий уведомлений studiosite\yii2pnotify
	*/
	public $popupOptions = [];

	/**
	* @var array Опции модальных окон  
	*/
	public $modalOptions = [];

	/**
	* @var array События 
	*/
	public $modalEvents = [];
}