<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use studiosite\yii2pnotify\Pnotify;

foreach($items as $key => $notification) {
    Modal::begin(ArrayHelper::merge([
        'header' => $notification->title ?: Yii::t('yii2notification', 'Notification'),
        'id' => 'notificationModal'.$notification->id,
        'footer' => Html::button(Yii::t('yii2notification', 'Close'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    ], $notification->modalOptions));

    echo ($notification->icon ? $notification->icon." " : '').$notification->message;

    Modal::end();

    if (is_array($notification->modalEvents)) {
       foreach ($notification->modalEvents as $eventName => $event) {
          $this->registerJs("

              jQuery('#notificationModal$notification->id').on('$eventName.bs.modal', $event);

          ");
       }
    }

    // Всплывающие уведомления
    Pnotify::widget(ArrayHelper::merge([
        "id" => 'notificationPopup'.$notification->id,
        "title" => $notification->title ?: Yii::t('yii2notification', 'Notification'),
        "text" => $notification->message.$this->context->renderButtons($notification->buttons),
        "type" => $notification->type ?: Pnotify::TYPE_NOTICE,
    ], $notification->popupOptions));
}



