yii2notification
=================

Всплывающие уведомления pnotify и bootstrap modal 
## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

```
"studiosite/yii2swipenav": "*"
```

в секции ```require``` `composer.json` файла.

## Использование

В отображении (Пример)

```php
/* @var $this \yii\web\View */
/* @var $content string */

use studiosite\yii2notification\Notification;
use studiosite\yii2notification\Model;
use yii\helpers\Html;

echo Notification::widget(['items' => [
  (new Model([
  		'id' => 1,
    	'title' => 'test',
    	'message' => 'test message',
    	'buttons' => Html::tag('div', 
                            Html::a('Details', '#', ['class' => 'btn btn-default', 'data-toggle' => 'modal', 'data-target' => '#notificationModal'.1]),
                            ['class' => 'text-right']),
    	// К модальному окну добавлены кнопки закрыть и ссылка на все уведомления
    	'modalOptions' => [
                            'footer' => Html::a('All notification', ['/system/user/profile'], ['class' => 'btn btn-primary'])." ".Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
                        ],
    	'modalEvents' => [
                            'show' => new \yii\web\JsExpression("function(e) {
                                // Указать статус прочитано
                                jQuery.get('/system/user-notification/read', {id: ".1."});
                                // Удаление popup
                                window.notificationPopup1.remove();
                            }")
                        ],
    	// Опции pnotify
    	'popupOptions' = [
            'timer' => false,
            'animation' => 'fade',
            'pluginOptions' => [
              'before_close' => (new JsExpression("function() {
                                          // Делаем запрос к контроллеру на изменение статуса на прочитанное
                                          jQuery.get('/system/user-notification/read', {id: ".$notification->id."});
                                      }")),
            ]
          ]
  ])) 
]]);
```
