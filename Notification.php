<?php

namespace studiosite\yii2notification;

use Yii;
use yii\base\Widget;
use yii\base\InvalidParamException;
use yii\bootstrap\Button;

/**
 * Виджет отрисовки уведомлений в виде всплывающих уведомлений и подробностей в модальном окне
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class Notification extends Widget
{
	/**
	* @var Model[]
	*/
	public $items = [];

	/**
    * Инициализация виджета
    * Подключение переводчика
    */
	public function init()
	{
        parent::init();

		Yii::setAlias('@studiosite/yii2notification', __DIR__);

		if (!isset(Yii::$app->i18n->translations['yii2notification'])) {
            Yii::$app->i18n->translations['yii2notification'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => '@studiosite/yii2notification/messages',
                'forceTranslation' => true,
                'fileMap' => [
                    'yii2notification' => 'messages.php'
                ]
            ];
        }
	}

	/**
	* Отрисовка
	*
	* @return string
	*/
	public function run()
	{
		if (!empty($this->items)) {
			foreach ($this->items as $item) {
				if (!$item instanceof Model) {
					throw new InvalidParamException(Yii::t('yii2notification' ,'Only {className} instance, configuration array or false is allowed', ['className' => 'studiosite\yii2notification\Model']));
				}
			}

			return $this->render('items', [
				'items' => $this->items
			]);
		}

		return '';
	}

	/**
	* Отрисовки кнопок
	*
	* @param array|string|Closure[]|string[]|array[] $buttons
	* @return string
	*/
	public function renderButtons($buttons = [])
	{
		if (empty($buttons))
			return '';

		if (is_string($buttons))
			return $buttons;

		if (is_array($buttons)) {
			$result = '';
			foreach ($buttons as $button) {
				if (is_callable($button))
					$result.= $button($this);
				else if (is_string($button)) {
					return $button;
				} else if (is_array($button)) {
					$result.= Button::widget($button);
				}
			}

			return $result;
		}

		return '';
	}
}